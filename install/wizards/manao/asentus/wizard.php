<?
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/install/wizard_sol/wizard.php");

class SelectSiteStep extends CSelectSiteWizardStep
{
    function InitStep()
    {
        parent::InitStep();

        $wizard =& $this->GetWizard();
        $wizard->solutionName = "asentus";
    }

}

class SelectTemplateStep extends CSelectTemplateWizardStep
{
	function InitStep()
	{
		$this->SetStepID("select_template");
		$this->SetTitle(GetMessage("SELECT_TEMPLATE_TITLE"));
		$this->SetSubTitle(GetMessage("SELECT_TEMPLATE_SUBTITLE"));
		if (!defined("WIZARD_DEFAULT_SITE_ID"))
		{
			$this->SetPrevStep("select_site");
			$this->SetPrevCaption(GetMessage("PREVIOUS_BUTTON"));
		}
		else
		{
			$wizard =& $this->GetWizard(); 
			$wizard->SetVar("siteID", WIZARD_DEFAULT_SITE_ID); 
		}

		$this->SetNextStep("site_settings");
		$this->SetNextCaption(GetMessage("NEXT_BUTTON"));
	}
}

class SiteSettingsStep extends CSiteSettingsWizardStep 
{

	function InitStep()
	{
		$wizard =& $this->GetWizard();
		$wizard->solutionName = "asentus";
		parent::InitStep();
		$this->SetTitle(GetMessage("wiz_settings"));
		$this->SetNextStep("data_install");
		$this->SetNextCaption(GetMessage("wiz_install"));

		$siteID = $wizard->GetVar("siteID");
		$templateID = $wizard->GetVar("templateID");
		
		global $USER;
		$AdminsMail=$USER->GetParam("EMAIL");

		$wizard->SetDefaultVars(
			Array(
				"siteName" => GetMessage("wiz_name"),
				"siteDescription" => GetMessage("wiz_slogan"), 
				"siteCopyright" => GetMessage("wisCopyright"), 
				"wiz_social_link_1_type" =>"Twitter",
				"wiz_social_link_2_type" =>"Facebook",
				"wiz_social_link_3_type" =>"Instagram",
				"wiz_social_link_4_type" =>"YouTube",
				"wiz_social_link_1"=>GetMessage("wiz_social_link_1"),
				"wiz_social_link_2"=>GetMessage("wiz_social_link_2"),
				"wiz_social_link_3"=>GetMessage("wiz_social_link_3"),
				"wiz_social_link_4"=>GetMessage("wiz_social_link_4"),
				"wiz_content_checkbox"=>"N",
				"admins_e_mail" => $AdminsMail,
			)
		);		
	}
	
	function ShowStep()
	{
		$wizard =& $this->GetWizard();
		$siteCopyright = $wizard->GetVar("siteCopyright", true);
		
		$this->content .= '<div class="wizard-input-form">';
		$this->content .= '
		<div class="wizard-upload-img-block">
			<div class="wizard-catalog-title">'.GetMessage("wiz_company_name").'</div>
			'.$this->ShowInputField('text', 'siteName', array("id" => "siteName", "class" => "wizard-field")).'
		</div>';
		$this->content .= '
		<div class="wizard-upload-img-block">
			<div class="wizard-catalog-title">'.GetMessage("wiz_company_description").'</div>
			'.$this->ShowInputField('text', 'siteDescription', array("id" => "siteDescription", "class" => "wizard-field")).'
		</div>';
		
		//START Add MAIL
		$this->content .= '
		<div class="wizard-upload-img-block">
			<div class="wizard-catalog-title">'.GetMessage("wiz_company_email").'</div>
			'.$this->ShowInputField('text', 'admins_e_mail', array("id" => "admins_e_mail", "class" => "wizard-field")).'
		</div>';
		//END Add MAIL
	
		//INTRO (create array) INPUT SOCIAL MENU POINTS
		$arConditions = array(
			"N" => GetMessage("wiz_social_link_0_text"),
			"Twitter" => GetMessage("wiz_social_link_1_text"),
			"Facebook" => GetMessage("wiz_social_link_2_text"),
			"Instagram" => GetMessage("wiz_social_link_3_text"),
			"YouTube" => GetMessage("wiz_social_link_4_text")	
		);

		foreach($arConditions as $conditionID => $conditionName)
		{
			$arReserveConditions[$conditionID] = $conditionName;
		}
		//START INPUT SOCIAL MENU POINTS
		$this->content .= 
			'<div class="wizard-catalog-title">'.GetMessage("wiz_social").'</div>'
				
			.'<table class="wizard-input-table"><tbody>'
				//START INPUT 1 SOCIAL MENU POINT
				.'<tr>'
					.'<td class="wizard-input-table-left">'.$this->ShowSelectField("wiz_social_link_1_type", $arReserveConditions, Array("id" => "wiz_social_link_1_type")).'</td>'				
					.'<td class="wizard-input-table-left"><label id="wiz_social_link_1_mess">'.GetMessage("wiz_social_link").'</label></td>'
					.'<td class="wizard-input-table-right">'.$this->ShowInputField('text', 'wiz_social_link_1', array("id" => "wiz_social_link_1", "class" => "wizard-field")).'</td>'
				.'</tr>'
				//END INPUT 1 SOCIAL MENU POINT
				.'<tr>'
					.'<td class="wizard-input-table-left">'.$this->ShowSelectField("wiz_social_link_2_type", $arReserveConditions, Array("id" => "wiz_social_link_2_type")).'</td>'
					.'<td class="wizard-input-table-left"><label id="wiz_social_link_2_mess">'.GetMessage("wiz_social_link").'</label></td>'
					.'<td class="wizard-input-table-right">'.$this->ShowInputField('text', 'wiz_social_link_2', array("id" => "wiz_social_link_2", "class" => "wizard-field")).'</td>'
				.'</tr>'
				.'<tr>'
					.'<td class="wizard-input-table-left">'.$this->ShowSelectField("wiz_social_link_3_type", $arReserveConditions, Array("id" => "wiz_social_link_3_type")).'</td>'
					.'<td class="wizard-input-table-left"><label id="wiz_social_link_3_mess">'.GetMessage("wiz_social_link").'</label></td>'
					.'<td class="wizard-input-table-right">'.$this->ShowInputField('text', 'wiz_social_link_3', array("id" => "wiz_social_link_3", "class" => "wizard-field")).'</td>'
				.'</tr>'
				.'<tr>'
					.'<td class="wizard-input-table-left">'.$this->ShowSelectField("wiz_social_link_4_type", $arReserveConditions, Array("id" => "wiz_social_link_4_type")).'</td>'
					.'<td class="wizard-input-table-left"><label id="wiz_social_link_4_mess">'.GetMessage("wiz_social_link").'</label></td>'
					.'<td class="wizard-input-table-right">'.$this->ShowInputField('text', 'wiz_social_link_4', array("id" => "wiz_social_link_4", "class" => "wizard-field")).'</td>'
				.'</tr>'
			.'</tbody></table>'
			
			//START FIDE SOCIAL MENU POINTS
			.'<script>
			document.getElementById("wiz_social_link_1_type").onchange = function(){
				if (this.value == "N") {
				document.getElementById("wiz_social_link_1").style.display="none";
				document.getElementById("wiz_social_link_1_mess").style.display="none";
				}
				else {
				document.getElementById("wiz_social_link_1").style.display="block";
				document.getElementById("wiz_social_link_1_mess").style.display="block";
				}
			}
			document.getElementById("wiz_social_link_2_type").onchange = function(){
				if (this.value == "N") {
				document.getElementById("wiz_social_link_2").style.display="none";
				document.getElementById("wiz_social_link_2_mess").style.display="none";
				}
				else {
				document.getElementById("wiz_social_link_2").style.display="block";
				document.getElementById("wiz_social_link_2_mess").style.display="block";
				}
			}
			document.getElementById("wiz_social_link_3_type").onchange = function(){
				if (this.value == "N") {
				document.getElementById("wiz_social_link_3").style.display="none";
				document.getElementById("wiz_social_link_3_mess").style.display="none";
				}
				else {
				document.getElementById("wiz_social_link_3").style.display="block";
				document.getElementById("wiz_social_link_3_mess").style.display="block";
				}
			}
			document.getElementById("wiz_social_link_4_type").onchange = function(){
				if (this.value == "N") {
				document.getElementById("wiz_social_link_4").style.display="none";
				document.getElementById("wiz_social_link_4_mess").style.display="none";
				}
				else {
				document.getElementById("wiz_social_link_4").style.display="block";
				document.getElementById("wiz_social_link_4_mess").style.display="block";
				}
			}
			</script>'
			//END FIDE SOCIAL MENU POINTS
		; 
		//END INPUT SOCIAL MENU POINTS
		$this->content .= '</div>';
		$wizard->SetVar("siteCopyright", GetMessage("wisCopyright"));
	}
}

class DataInstallStep extends CDataInstallWizardStep
{

}

class FinishStep extends CFinishWizardStep
{
    function InitStep()
    {
        $this->SetStepID("finish");
        $this->SetNextStep("finish");
        $this->SetTitle(GetMessage("FINISH_STEP_TITLE"));
        $this->SetNextCaption(GetMessage("wiz_go"));
    }

    function ShowStep()
    {
        global $USER;
        $wizard =& $this->GetWizard();

        $siteID = WizardServices::GetCurrentSiteID($wizard->GetVar("siteID"));

        if (strlen($siteID) > 0 and is_object($USER) and method_exists($USER, 'GetEmail')) {
            $obSite = new CSite();
            $t = $obSite->Update($siteID, array(
                'EMAIL' => $USER->GetEmail(),
                'NAME' => GetMessage('wiz_site_name'),
                'SERVER_NAME' => $this->getSiteUrl()
            ));
        };

        $rsSites = CSite::GetByID($siteID);
        $siteDir = SITE_DIR;
        if ($arSite = $rsSites->Fetch())
            $siteDir = $arSite["DIR"];

        $wizard->SetFormActionScript(str_replace("//", "/", $siteDir . "/?finish"));

        $this->CreateNewIndex();

        COption::SetOptionString("main", "wizard_solution", $wizard->solutionName, false, $siteID);

        $this->content .= GetMessage("FINISH_STEP_COMPOSITE");
		$this->content .= '<br/><center><a href="/bitrix/admin/composite.php?lang=ru">'.CFile::ShowImage("/bitrix/wizards/manao/asentus/images/ru/composite.gif", 600, 250, "border=0 vspace=15").'</a></center>';
		$this->content .= '<center><b><a href="/bitrix/admin/composite.php?lang=ru">'.GetMessage("FINISH_STEP_COMPOSITE_LINK").'</a></b></center>';
	
    }

    function getSiteUrl()
    {
        $PARSE_HOST = parse_url(getenv('HTTP_HOST'));
        if (isset($PARSE_HOST['port']) and $PARSE_HOST['port'] == '80') {
            $HOST = $PARSE_HOST['host'];
        }
        elseif (isset($PARSE_HOST['port']) and $PARSE_HOST['port'] == '443') {
            $HOST = $PARSE_HOST['host'];
        }
        elseif(isset($PARSE_HOST['port'])) {
            $HOST = $PARSE_HOST['host'] . ":" . $PARSE_HOST['port'];
        } else {
            $HOST = $PARSE_HOST['host'];
        }
        return $HOST;
    }
}

?>