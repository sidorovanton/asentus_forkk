<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="section-seperator">
	<div class="content-lg container">
		<div class="row">
<? foreach ($arResult["ITEMS"] as $arItem): ?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
	<div class="col-sm-4 sm-margin-b-50" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
		<div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".3s">
			<? if ($arItem["NAME"]): ?>
				<h3>
					<a href="#"><?= $arItem["NAME"] ?></a> 
					<? if ($arItem["DISPLAY_PROPERTIES"]["CONTACT_SPECIFICATION"]) : ?>
						<span class="text-uppercase margin-l-20"><?= $arItem["DISPLAY_PROPERTIES"]["CONTACT_SPECIFICATION"]["DISPLAY_VALUE"]?></span>
					<? endif; ?>
				</h3>
            <? endif; ?>
			<? if (strlen($arItem["PREVIEW_TEXT"]) > 0): ?>
                <p><?= $arItem["PREVIEW_TEXT"] ?></p>
            <? endif; ?>
			<ul class="list-unstyled contact-list">
				<? if ($arItem["DISPLAY_PROPERTIES"]["CONTACT_PHONE"]) : ?>
					<? $Phone = $arItem["DISPLAY_PROPERTIES"]["CONTACT_PHONE"]["DISPLAY_VALUE"];?>
					<li><i class="margin-r-10 color-base icon-call-out"></i><a href="tel:+<?= $Phone ?>"><?= $Phone ?></a></li>
				<? endif; ?>
				<? if ($arItem["DISPLAY_PROPERTIES"]["CONTACT_EMAIL"]) : ?>
					<? $Email = $arItem["DISPLAY_PROPERTIES"]["CONTACT_EMAIL"]["DISPLAY_VALUE"];?>
					<li><i class="margin-r-10 color-base icon-envelope"></i> <a href="mailto:<?= $Email ?>"><?= $Email ?></a></li>
				<? endif; ?>
			</ul>
		</div>
	</div>
<? endforeach; ?>
		</div>
	</div>
</div>

			
			
