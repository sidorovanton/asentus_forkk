<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="bg-color-sky-light">
	<div class="content-lg container">
		<div class="swiper-slider swiper-clients">
			<div class="swiper-wrapper">
<? foreach ($arResult["ITEMS"] as $arItem): ?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
	<? if (is_array($arItem["DISPLAY_PROPERTIES"]["CLIENT_LOGO"]["FILE_VALUE"])) : ?>
		<? $src = $arItem["DISPLAY_PROPERTIES"]["CLIENT_LOGO"]["FILE_VALUE"]["SRC"];?>
		<div class="swiper-slide" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
			<? if (is_array($arItem["DISPLAY_PROPERTIES"]["CLIENT_LINK"])) : ?>
				<a href="<?= $arItem["DISPLAY_PROPERTIES"]["CLIENT_LINK"]["VALUE"] ?>" target="_blank">
			<? endif; ?>
					<img
						<? if ($arItem["NAME"]): ?>
							alt="<?= $arItem["NAME"] ?>"
						<? endif; ?>
						src="<?=$src?>"
						class="swiper-clients-img">
			<? if (is_array($arItem["DISPLAY_PROPERTIES"]["CLIENT_LINK"])) : ?>
				</a>
			<? endif; ?>
		</div>
	<? endif; ?>
<? endforeach; ?>
			</div>
		</div>
	</div>
</div>

