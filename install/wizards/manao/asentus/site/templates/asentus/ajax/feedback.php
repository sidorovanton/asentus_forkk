<? require_once($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/include/prolog_before.php');
global $APPLICATION;
$APPLICATION->IncludeComponent(
	"manao:feedback",
	"footer",
	array(
		"USE_CAPTCHA" => "N",
		"OK_TEXT" => $APPLICATION->GetProperty("OK_TEXT"),
		"EMAIL_TO" => $APPLICATION->GetProperty("admins_e_mail"),
		"REQUIRED_FIELDS" => array(
			0 => "NAME",
			1 => "EMAIL",
			2 => "MESSAGE",
			3 => "PHONE",
		),
		"EVENT_MESSAGE_ID" => array(
			0 => "8",
		),
		"COMPONENT_TEMPLATE" => "footer"
	),
	false
); ?>