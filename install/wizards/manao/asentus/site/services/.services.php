<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arServices = Array(
	'main' => Array(
		'NAME' => GetMessage("SERVICE_MAIN_SETTINGS"),
		'STAGES' => Array(
			"files.php",
			"template.php",
			"settings.php",
			"post_event.php",
			"php_interface.php"
		),
	),
	"iblock" => array(
		"NAME" => GetMessage("SERVICE_IBLOCK"),
		"STAGES" => array(
			"types.php",
			"about.php",
			"faq.php",
			"pricing.php",
			"products.php",
			"reviews.php",
			"services.php",
			"slider.php",
			"team.php",
			"clients.php",
			"contacts.php",
		),
	),
);
?>
