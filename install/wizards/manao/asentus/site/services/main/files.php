<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

if (!defined("WIZARD_SITE_ID"))
    return;

if (!defined("WIZARD_SITE_DIR"))
    return;

$path = str_replace("//", "/", WIZARD_ABSOLUTE_PATH . "/site/public/" . LANGUAGE_ID . "/");

$handle = @opendir($path);
if ($handle) {
    while ($file = readdir($handle)) {
        if (in_array($file, array(".","..","bitrix_php_interface"))) 
			continue;
        CopyDirFiles(
            $path . $file,
            WIZARD_SITE_PATH . "/" . $file,
            $rewrite = true,
            $recursive = true,
            $delete_after_copy = false
        );
    }

}

CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/_index.php", array(
	"SITE_TITLE" => htmlspecialcharsbx($wizard->GetVar("siteName")),
	"SITE_DESCRIPTION" => htmlspecialcharsbx($wizard->GetVar("siteDescription")),
));

//START CREATE SOCIAL MENU POINT
$social_link_1=htmlspecialcharsbx($wizard->GetVar("wiz_social_link_1"));
$social_link_1_type=htmlspecialcharsbx($wizard->GetVar("wiz_social_link_1_type"));

$social_link_2=htmlspecialcharsbx($wizard->GetVar("wiz_social_link_2"));
$social_link_2_type=htmlspecialcharsbx($wizard->GetVar("wiz_social_link_2_type"));

$social_link_3=htmlspecialcharsbx($wizard->GetVar("wiz_social_link_3"));
$social_link_3_type=htmlspecialcharsbx($wizard->GetVar("wiz_social_link_3_type"));

$social_link_4=htmlspecialcharsbx($wizard->GetVar("wiz_social_link_4"));
$social_link_4_type=htmlspecialcharsbx($wizard->GetVar("wiz_social_link_4_type"));

if ($social_link_1_type == "N" and $social_link_2_type == "N" and $social_link_3_type == "N" and $social_link_4_type == "N")
{
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/include/social_networks_list.php", array(
		"SOCIAL_LINKS" => "",
	));
}
else 
{
	$content .= '<ul class="list-unstyled footer-list">';
	if (!empty($social_link_1) and $social_link_1_type!="N")
		$content .= '<li class="footer-list-item"><a class="footer-list-link" href="'.$social_link_1.'" target="_blank">'.$social_link_1_type.'</a></li>';
	if (!empty($social_link_2) and $social_link_2_type!="N")
		$content .= '<li class="footer-list-item"><a class="footer-list-link" href="'.$social_link_2.'" target="_blank">'.$social_link_2_type.'</a></li>';
	if (!empty($social_link_3) and $social_link_3_type!="N")
		$content .= '<li class="footer-list-item"><a class="footer-list-link" href="'.$social_link_3.'" target="_blank">'.$social_link_3_type.'</a></li>';
	if (!empty($social_link_4) and $social_link_4_type!="N")
		$content .= '<li class="footer-list-item"><a class="footer-list-link" href="'.$social_link_4.'" target="_blank">'.$social_link_4_type.'</a></li>';
	$content .= '</ul>';
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/include/social_networks_list.php", array(
		"SOCIAL_LINKS" => $content,
	));
}
//END CREATE SOCIAL MENU POINT
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/products/index.php", array(
	"SITE_NAME" => WIZARD_SITE_DIR,
));
?>